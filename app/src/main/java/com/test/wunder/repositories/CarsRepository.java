package com.test.wunder.repositories;

import com.test.wunder.appmodels.Car;
import com.test.wunder.appmodels.CarDetails;
import com.test.wunder.rest.RestCallFactory;
import com.test.wunder.rest.requestmodels.RentCarRequest;
import com.test.wunder.rest.responsemodels.CarResponse;
import com.test.wunder.rest.responsemodels.CarDetailsResponse;
import com.test.wunder.rest.responsemodels.RentCarResponse;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.functions.Function;

public class CarsRepository {

    private RestCallFactory callFactory = new RestCallFactory();

    public Single<List<Car>> loadCars() {
        return callFactory.getCars()
                .flatMap(new Function<List<CarResponse>, SingleSource<? extends List<Car>>>() {
                    @Override
                    public SingleSource<? extends List<Car>> apply(List<CarResponse> apiCars) {
                        List<Car> cars = new ArrayList<>();
                        for (CarResponse apiCar : apiCars)
                            cars.add(new Car(apiCar.getCarId(), apiCar.getTitle(), apiCar.getLat(), apiCar.getLon()));
                        return Single.just(cars);
                    }
                });
    }

    public Single<CarDetails> getCarDetails(int carId) {
        return callFactory.getCarDetails(carId)
                .flatMap(new Function<CarDetailsResponse, SingleSource<? extends CarDetails>>() {
                    @Override
                    public SingleSource<? extends CarDetails> apply(CarDetailsResponse carDetailsResponse) throws Exception {
                        return Single.just(CarDetails.createFrom(carDetailsResponse));
                    }
                });
    }

    public Single<RentCarResponse> rentCar(RentCarRequest body) {
        return callFactory.rentACar(body);
    }

}
