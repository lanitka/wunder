package com.test.wunder.compositeviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TableLayout;
import android.widget.TextView;

import com.test.wunder.R;
import com.test.wunder.appmodels.CarDetails;

public class CarDetailsView extends TableLayout {

    public CarDetailsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.car_details_view, this);
    }

    public void displayCarDetails(CarDetails carDetails) {
        ((TextView)findViewById(R.id.car_details_id)).setText(String.valueOf(carDetails.getCarId()));
        ((TextView)findViewById(R.id.car_details_title)).setText(carDetails.getTitle());
        ((TextView)findViewById(R.id.car_details_isClean)).setText(carDetails.isClean() ? R.string.yes : R.string.no);
        ((TextView)findViewById(R.id.car_details_isDamaged)).setText((carDetails.isDamaged()) ? R.string.yes : R.string.no);
        ((TextView)findViewById(R.id.car_details_damage_ds)).setText(carDetails.getDamageDescription());
        ((TextView)findViewById(R.id.car_details_licence_plate)).setText(carDetails.getLicencePlate());
        ((TextView)findViewById(R.id.car_details_fuel_level)).setText(String.valueOf(carDetails.getFuelLevel()));
        ((TextView)findViewById(R.id.car_details_state_id)).setText(String.valueOf(carDetails.getVehicleStateId()));
        ((TextView)findViewById(R.id.car_details_hardware_id)).setText(carDetails.getHardwareId());
        ((TextView)findViewById(R.id.car_details_type_id)).setText(String.valueOf(carDetails.getVehicleTypeId()));
        ((TextView)findViewById(R.id.car_details_pricing_time)).setText(carDetails.getPricingTime());
        ((TextView)findViewById(R.id.car_details_pricing_parking)).setText(carDetails.getPricingParking());
        ((TextView)findViewById(R.id.car_details_hardware_activated)).setText(
                carDetails.isActivatedByHardware() ? R.string.yes : R.string.no);
        ((TextView)findViewById(R.id.car_details_location_id)).setText(String.valueOf(carDetails.getLocationId()));
        ((TextView)findViewById(R.id.car_details_address)).setText(carDetails.getAddress());
        ((TextView)findViewById(R.id.car_details_zipcode)).setText(carDetails.getZipCode());
        ((TextView)findViewById(R.id.car_details_city)).setText(carDetails.getCity());
        ((TextView)findViewById(R.id.car_details_lat)).setText(String.valueOf(carDetails.getLat()));
        ((TextView)findViewById(R.id.car_details_lon)).setText(String.valueOf(carDetails.getLon()));
        ((TextView)findViewById(R.id.car_details_reservation_state)).setText(String.valueOf(carDetails.getReservationState()));

    }
}
