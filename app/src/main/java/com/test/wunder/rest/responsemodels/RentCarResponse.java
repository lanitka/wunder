package com.test.wunder.rest.responsemodels;

public class RentCarResponse {
    private int carId;
    private double cost;
    private double drivenDistance;
    private String licencePlate;
    private String startAddress;
    private int userId;
    private boolean isParkModeEnabled;
    private String damageDescription;
    private int fuelCardPin;
    private long endTime;
    private long startTime;

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getDrivenDistance() {
        return drivenDistance;
    }

    public void setDrivenDistance(double drivenDistance) {
        this.drivenDistance = drivenDistance;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isParkModeEnabled() {
        return isParkModeEnabled;
    }

    public void setParkModeEnabled(boolean parkModeEnabled) {
        isParkModeEnabled = parkModeEnabled;
    }

    public String getDamageDescription() {
        return damageDescription;
    }

    public void setDamageDescription(String damageDescription) {
        this.damageDescription = damageDescription;
    }

    public int getFuelCardPin() {
        return fuelCardPin;
    }

    public void setFuelCardPin(int fuelCardPin) {
        this.fuelCardPin = fuelCardPin;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
}
