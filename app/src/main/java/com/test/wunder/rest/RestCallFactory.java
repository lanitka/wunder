package com.test.wunder.rest;

import com.test.wunder.BuildConfig;
import com.test.wunder.rest.requestmodels.RentCarRequest;
import com.test.wunder.rest.responsemodels.CarResponse;
import com.test.wunder.rest.responsemodels.CarDetailsResponse;
import com.test.wunder.rest.responsemodels.RentCarResponse;
import com.test.wunder.utils.Logger;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestCallFactory {
    private Logger logger = new Logger(RestCallFactory.class);

    private static final String BASE_URL = "https://s3.eu-central-1.amazonaws.com/wunderfleet-recruiting-dev/";
    private static final String BASE_URL_RENT_CAR = "https://4i96gtjfia.execute-api.eu-central-1.amazonaws.com/default/";

    private RestService restService;
    private RestService rentACarRestService;

    {
        HttpLoggingInterceptor.Level loggingLevel;
        if(BuildConfig.DEBUG)
            loggingLevel = HttpLoggingInterceptor.Level.BODY;
        else
            loggingLevel = HttpLoggingInterceptor.Level.NONE;

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if(loggingLevel != HttpLoggingInterceptor.Level.NONE) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(loggingLevel);
            builder.addInterceptor(loggingInterceptor);
        }
        restService = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build()
                .create(RestService.class);
        rentACarRestService = new Retrofit.Builder()
                .baseUrl(BASE_URL_RENT_CAR)
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build()
                .create(RestService.class);

    }

    public Single<List<CarResponse>> getCars() {
        return Single.create(
                new SingleOnSubscribe<List<CarResponse>>() {
                    @Override
                    public void subscribe(final SingleEmitter<List<CarResponse>> emitter) {
                        restService.getCars()
                                .enqueue(new Callback<List<CarResponse>>() {
                                    @Override
                                    public void onResponse(Call<List<CarResponse>> call, Response<List<CarResponse>> response) {
                                        logger.debug("getCars. onResponse");
                                        emitter.onSuccess(response.body());
                                    }

                                    @Override
                                    public void onFailure(Call<List<CarResponse>> call, Throwable t) {
                                        logger.error("getCars. onFailure", t);
                                        emitter.onError(t);
                                    }
                                });
                    }
                }
        );
    }

    public Single<CarDetailsResponse> getCarDetails(final int carId) {
        return Single.create(
                new SingleOnSubscribe<CarDetailsResponse>() {
                    @Override
                    public void subscribe(final SingleEmitter<CarDetailsResponse> emitter) {
                        restService.getCarDetails(carId)
                                .enqueue(new Callback<CarDetailsResponse>() {
                                    @Override
                                    public void onResponse(Call<CarDetailsResponse> call, Response<CarDetailsResponse> response) {
                                        logger.debug("getCarDetails. onResponse");
                                        emitter.onSuccess(response.body());
                                    }

                                    @Override
                                    public void onFailure(Call<CarDetailsResponse> call, Throwable t) {
                                        logger.error("getCarDetails. onFailure", t);
                                        emitter.onError(t);
                                    }
                                });
                    }
                }
        );
    }

    public Single<RentCarResponse> rentACar(final RentCarRequest body) {
        return Single.create(new SingleOnSubscribe<RentCarResponse>() {
            @Override
            public void subscribe(final SingleEmitter<RentCarResponse> emitter) {
                rentACarRestService.rentACar(body)
                        .enqueue(new Callback<RentCarResponse>() {
                            @Override
                            public void onResponse(Call<RentCarResponse> call, Response<RentCarResponse> response) {
                                logger.debug("rentACar. onResponse");
                                emitter.onSuccess(response.body());
                            }

                            @Override
                            public void onFailure(Call<RentCarResponse> call, Throwable t) {
                                logger.error("rentACar. onFailure", t);
                                emitter.onError(t);
                            }
                        });
            }
        });
    }

}
