package com.test.wunder.rest;

import com.test.wunder.rest.requestmodels.RentCarRequest;
import com.test.wunder.rest.responsemodels.CarResponse;
import com.test.wunder.rest.responsemodels.CarDetailsResponse;
import com.test.wunder.rest.responsemodels.RentCarResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RestService {

    @GET("cars.json")
    Call<List<CarResponse>> getCars();

    @GET("cars/{carId}")
    Call<CarDetailsResponse> getCarDetails(@Path("carId") int carId);

    @Headers("Authorization: Bearer df7c313b47b7ef87c64c0f5f5cebd6086bbb0fa")
    @POST("wunderfleet-recruiting-mobile-dev-quick-rental")
    Call<RentCarResponse> rentACar(@Body RentCarRequest body);

}
