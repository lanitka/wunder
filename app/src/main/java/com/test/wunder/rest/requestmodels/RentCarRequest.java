package com.test.wunder.rest.requestmodels;

public class RentCarRequest {
    private int carId;

    public RentCarRequest(int carId) {
        this.carId = carId;
    }

    public int getCarId() {
        return carId;
    }
}
