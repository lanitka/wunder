package com.test.wunder.rest.responsemodels;

public class CarDetailsResponse {

    private int carId;
    private String title;
    private boolean isClean;
    private boolean isDamaged;
    private String licencePlate;
    private int fuelLevel;
    private int vehicleStateId;
    private String hardwareId;
    private int vehicleTypeId;
    private String pricingTime;
    private String pricingParking;
    private boolean isActivatedByHardware;
    private int locationId;
    private String address;
    private String zipCode;
    private String city;
    private double lat;
    private double lon;
    private int reservationState;
    private String damageDescription;
    private String vehicleTypeImageUrl;


    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isClean() {
        return isClean;
    }

    public void setClean(boolean clean) {
        isClean = clean;
    }

    public boolean isDamaged() {
        return isDamaged;
    }

    public void setDamaged(boolean damaged) {
        isDamaged = damaged;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public int getFuelLevel() {
        return fuelLevel;
    }

    public void setFuelLevel(int fuelLevel) {
        this.fuelLevel = fuelLevel;
    }

    public int getVehicleStateId() {
        return vehicleStateId;
    }

    public void setVehicleStateId(int vehicleStateId) {
        this.vehicleStateId = vehicleStateId;
    }

    public String getHardwareId() {
        return hardwareId;
    }

    public void setHardwareId(String hardwareId) {
        this.hardwareId = hardwareId;
    }

    public int getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(int vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getPricingTime() {
        return pricingTime;
    }

    public void setPricingTime(String pricingTime) {
        this.pricingTime = pricingTime;
    }

    public String getPricingParking() {
        return pricingParking;
    }

    public void setPricingParking(String pricingParking) {
        this.pricingParking = pricingParking;
    }

    public boolean isActivatedByHardware() {
        return isActivatedByHardware;
    }

    public void setActivatedByHardware(boolean activatedByHardware) {
        isActivatedByHardware = activatedByHardware;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public int getReservationState() {
        return reservationState;
    }

    public void setReservationState(int reservationState) {
        this.reservationState = reservationState;
    }

    public String getDamageDescription() {
        return damageDescription;
    }

    public void setDamageDescription(String damageDescription) {
        this.damageDescription = damageDescription;
    }

    public String getVehicleTypeImageUrl() {
        return vehicleTypeImageUrl;
    }

    public void setVehicleTypeImageUrl(String vehicleTypeImageUrl) {
        this.vehicleTypeImageUrl = vehicleTypeImageUrl;
    }
}
