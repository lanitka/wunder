package com.test.wunder.appmodels;


public class Car {
    private int carId;
    private String title;
    private double lat;
    private double lon;

    public Car(int carId, String title, double lat, double lon) {
        this.carId = carId;
        this.title = title;
        this.lat = lat;
        this.lon = lon;
    }

    public int getCarId() {
        return carId;
    }

    public String getTitle() {
        return title;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return carId == car.carId;
    }

    @Override
    public int hashCode() {
        return carId;
    }
}
