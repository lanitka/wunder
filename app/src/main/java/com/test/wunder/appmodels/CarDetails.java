package com.test.wunder.appmodels;

import com.test.wunder.rest.responsemodels.CarDetailsResponse;

public class CarDetails {
    private int carId;
    private String title;
    private boolean isClean;
    private boolean isDamaged;
    private String licencePlate;
    private int fuelLevel;
    private int vehicleStateId;
    private String hardwareId;
    private int vehicleTypeId;
    private String pricingTime;
    private String pricingParking;
    private boolean isActivatedByHardware;
    private int locationId;
    private String address;
    private String zipCode;
    private String city;
    private double lat;
    private double lon;
    private int reservationState;
    private String damageDescription;
    private String vehicleTypeImageUrl;

    private CarDetails(int carId, String title, boolean isClean, boolean isDamaged, String licencePlate, int fuelLevel,
                       int vehicleStateId, String hardwareId, int vehicleTypeId, String pricingTime, String pricingParking,
                       boolean isActivatedByHardware, int locationId, String address, String zipCode, String city,
                       double lat, double lon, int reservationState, String damageDescription, String vehicleTypeImageUrl) {
        this.carId = carId;
        this.title = title;
        this.isClean = isClean;
        this.isDamaged = isDamaged;
        this.licencePlate = licencePlate;
        this.fuelLevel = fuelLevel;
        this.vehicleStateId = vehicleStateId;
        this.hardwareId = hardwareId;
        this.vehicleTypeId = vehicleTypeId;
        this.pricingTime = pricingTime;
        this.pricingParking = pricingParking;
        this.isActivatedByHardware = isActivatedByHardware;
        this.locationId = locationId;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.lat = lat;
        this.lon = lon;
        this.reservationState = reservationState;
        this.damageDescription = damageDescription;
        this.vehicleTypeImageUrl = vehicleTypeImageUrl;
    }

    public int getCarId() {
        return carId;
    }

    public String getTitle() {
        return title;
    }

    public boolean isClean() {
        return isClean;
    }

    public boolean isDamaged() {
        return isDamaged;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public int getFuelLevel() {
        return fuelLevel;
    }

    public int getVehicleStateId() {
        return vehicleStateId;
    }

    public String getHardwareId() {
        return hardwareId;
    }

    public int getVehicleTypeId() {
        return vehicleTypeId;
    }

    public String getPricingTime() {
        return pricingTime;
    }

    public String getPricingParking() {
        return pricingParking;
    }

    public boolean isActivatedByHardware() {
        return isActivatedByHardware;
    }

    public int getLocationId() {
        return locationId;
    }

    public String getAddress() {
        return address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getCity() {
        return city;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public int getReservationState() {
        return reservationState;
    }

    public String getDamageDescription() {
        return damageDescription;
    }

    public String getVehicleTypeImageUrl() {
        return vehicleTypeImageUrl;
    }

    public static CarDetails createFrom(CarDetailsResponse carDetailsResponse) {
        return new CarDetails(carDetailsResponse.getCarId(),
                carDetailsResponse.getTitle(), carDetailsResponse.isClean(), carDetailsResponse.isDamaged(),
                carDetailsResponse.getLicencePlate(), carDetailsResponse.getFuelLevel(), carDetailsResponse.getVehicleStateId(),
                carDetailsResponse.getHardwareId(), carDetailsResponse.getVehicleTypeId(), carDetailsResponse.getPricingTime(),
                carDetailsResponse.getPricingParking(), carDetailsResponse.isActivatedByHardware(), carDetailsResponse.getLocationId(),
                carDetailsResponse.getAddress(), carDetailsResponse.getZipCode(), carDetailsResponse.getCity(), carDetailsResponse.getLat(),
                carDetailsResponse.getLon(), carDetailsResponse.getReservationState(), carDetailsResponse.getDamageDescription(),
                carDetailsResponse.getVehicleTypeImageUrl());
    }
}
