package com.test.wunder.screens.cardetails;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.test.wunder.R;
import com.test.wunder.appmodels.CarDetails;
import com.test.wunder.compositeviews.CarDetailsView;
import com.test.wunder.utils.Logger;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class CarDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private Logger logger = new Logger(CarDetailsActivity.class);

    private ProgressBar progressBar;
    private Button btnRent;
    private CarDetailsView carDetailsView;
    private CarDetailsViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_details);

        progressBar = findViewById(R.id.car_details_progress);
        carDetailsView = findViewById(R.id.car_details_details);
        btnRent = findViewById(R.id.car_details_btn_rent);
        btnRent.setOnClickListener(this);

        viewModel = ViewModelProviders.of(this).get(CarDetailsViewModel.class);
        getLifecycle().addObserver(viewModel);

        registerViewModelObservers();

        viewModel.getCarDetails(extractCarId());
    }

    private void registerViewModelObservers() {
        viewModel.isLoading.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isLoading) {
                progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
            }
        });

        viewModel.carDetailsLiveData.observe(this, new Observer<CarDetails>() {
            @Override
            public void onChanged(CarDetails carDetails) {
                logger.debug("carDetails. onChanged. carDetails loaded " + (carDetails != null));
                if(carDetails != null) {
                    carDetailsView.displayCarDetails(carDetails);
                    ImageView carPic = findViewById(R.id.car_details_image);
                    Picasso.with(CarDetailsActivity.this).load(carDetails.getVehicleTypeImageUrl()).into(carPic);
                } else
                    Toast.makeText(CarDetailsActivity.this, R.string.error_load_car_details, Toast.LENGTH_SHORT).show();
            }
        });

        viewModel.rentIsPossible.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean rentIsPossible) {
                btnRent.setEnabled(rentIsPossible);
            }
        });

        viewModel.rideStarted.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean started) {
                Toast.makeText(CarDetailsActivity.this, started ? R.string.success_start_ride :
                        R.string.error_start_ride, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private int extractCarId() {
        return getIntent().getIntExtra(carIdKey, -1);
    }

    public static final String carIdKey = "carId";

    @Override
    public void onClick(View v) {
        viewModel.rentACar();
    }
}
