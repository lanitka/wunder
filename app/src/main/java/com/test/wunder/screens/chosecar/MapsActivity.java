package com.test.wunder.screens.chosecar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.test.wunder.R;
import com.test.wunder.appmodels.Car;
import com.test.wunder.utils.Logger;
import com.test.wunder.utils.Navigator;
import com.test.wunder.utils.PermissionUtils;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    private Logger logger = new Logger(MapsActivity.class);

    private GoogleMap mMap;
    private MapViewModel viewModel;
    private List<Marker> markers = new ArrayList<>();

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        viewModel = ViewModelProviders.of(this).get(MapViewModel.class);
        getLifecycle().addObserver(viewModel);
        viewModel.carsLiveData.observe(this, new Observer<List<Car>>() {
            @Override
            public void onChanged(List<Car> cars) {
                logger.debug("CarsLiveData sent update");
                addCarsOnMap(cars);
            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void addCarsOnMap(List<Car> cars) {
        if(mMap == null) {
            logger.warn("addCarsOnMap. failed. mMap = null");
            return;
        }
        markers.clear();
        mMap.clear();
        if(viewModel.selectedCar != null)
            addMarkerForCar(viewModel.selectedCar);
        else
            for(Car car : cars)
                addMarkerForCar(car);
    }

    private void addMarkerForCar(Car car) {
        Marker marker = mMap.addMarker(new MarkerOptions()
        .position(new LatLng(car.getLat(), car.getLon()))
        .title(car.getTitle()));
        marker.setTag(car.getCarId());
        markers.add(marker);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if(viewModel.selectedCar == null) {
            logger.debug("onMarkerClick. markerTag " + marker.getTag());
            viewModel.selectCarById((int) marker.getTag());
            for (Marker m : markers) {
                if (m.getTag() != marker.getTag())
                    m.remove();
            }
            markers.clear();
            markers.add(marker);
        } else {
            logger.debug("onMarkerTwiceClick. markerTag " + marker.getTag());
            Navigator.navigateToCarDetails(this, viewModel.selectedCar.getCarId());
        }
        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        logger.debug("onMapClick");
        viewModel.selectedCar = null;
        List<Car> cars = viewModel.carsLiveData.getValue();
        if(cars != null) {
            if(cars.size() != markers.size())
                addCarsOnMap(cars);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        logger.debug("onMapReady");
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(this);
        viewModel.getCars();
        showUserLocation();
    }

    private void showUserLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if(mMap != null) {
                mMap.setMyLocationEnabled(true);
                manageMapPosition();
            }
        }
        else
            PermissionUtils.askForPermission(findViewById(android.R.id.content), this, LOCATION_PERMISSION_REQUEST_CODE);
    }

    private void manageMapPosition() {
        if(viewModel.selectedCar != null)
            centerMapOnSelectedCar();
        else
            centerMapOnUserLocation();
    }

    private void centerMapOnUserLocation() {
        logger.debug("centerMapOnUserLocation");
        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(this);
        client.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                CameraPosition cp = CameraPosition.builder()
                        .target(new LatLng(location.getLatitude(), location.getLongitude()))
                        .zoom(9)
                        .build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp));
            }
        });
    }

    private void centerMapOnSelectedCar() {
        logger.debug("centerMapOnSelectedCar");
        CameraPosition cp = CameraPosition.builder()
                .target(new LatLng(viewModel.selectedCar.getLat(), viewModel.selectedCar.getLon()))
                .zoom(15)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode != LOCATION_PERMISSION_REQUEST_CODE)
            return;
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED))
            showUserLocation();
        else
            PermissionUtils.showLocationNeededSnackbar(findViewById(android.R.id.content), this, LOCATION_PERMISSION_REQUEST_CODE);
    }
}
