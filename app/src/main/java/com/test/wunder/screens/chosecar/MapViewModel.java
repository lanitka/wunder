package com.test.wunder.screens.chosecar;


import com.test.wunder.appmodels.Car;
import com.test.wunder.repositories.CarsRepository;
import com.test.wunder.screens.ParentViewModel;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MapViewModel extends ParentViewModel {

    private Map<Integer, Car> carsMap = new HashMap<>();
    MutableLiveData<List<Car>> carsLiveData = new MutableLiveData<>();
    Car selectedCar;

    void getCars() {
        if(carsMap.isEmpty()) {
            Disposable d = new CarsRepository().loadCars()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<List<Car>>() {
                                   @Override
                                   public void accept(List<Car> cars) {
                                        carsMap.clear();
                                        generateCarsMap(cars);
                                        carsLiveData.setValue(new LinkedList(carsMap.values()));
                                   }
                               },
                            new Consumer<Throwable>() {
                                @Override
                                public void accept(Throwable throwable) {
                                }
                            });
            destroyDisposable.add(d);
        } else
            carsLiveData.setValue(new LinkedList(carsMap.values()));
    }

    private void generateCarsMap(List<Car> list) {
        for(Car car : list)
            carsMap.put(car.getCarId(), car);
    }

    void selectCarById(int carId) {
        selectedCar = carsMap.get(carId);
    }
}
