package com.test.wunder.screens;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;

public class ParentViewModel extends ViewModel implements LifecycleObserver {

    protected CompositeDisposable destroyDisposable = new CompositeDisposable();

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void clearDestroyDisposable() {
        destroyDisposable.clear();
    }

}
