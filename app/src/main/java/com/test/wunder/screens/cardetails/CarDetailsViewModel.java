package com.test.wunder.screens.cardetails;

import com.test.wunder.appmodels.CarDetails;
import com.test.wunder.repositories.CarsRepository;
import com.test.wunder.rest.requestmodels.RentCarRequest;
import com.test.wunder.rest.responsemodels.RentCarResponse;
import com.test.wunder.screens.ParentViewModel;
import com.test.wunder.utils.Logger;

import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class CarDetailsViewModel extends ParentViewModel {

    private Logger logger = new Logger(CarDetailsViewModel.class);

    MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    MutableLiveData<CarDetails> carDetailsLiveData = new MutableLiveData<>();
    MutableLiveData<Boolean> rentIsPossible = new MutableLiveData<>();
    MutableLiveData<Boolean> rideStarted = new MutableLiveData<>();
    private CarDetails carDetails;

    void getCarDetails(int carId) {
        logger.debug("getCarDetails. alreadyLoaded = " + (carDetails != null));
        if(carDetails == null) {
            isLoading.setValue(true);
            Disposable d = new CarsRepository().getCarDetails(carId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<CarDetails>() {
                        @Override
                        public void accept(CarDetails carDetails) {
                            logger.debug("car details loaded");
                            isLoading.setValue(false);
                            processNewCarDetails(carDetails);
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) {
                            logger.error("car details not loaded " + throwable.getMessage());
                            isLoading.setValue(false);
                            processNewCarDetails(null);
                        }
                    });
            destroyDisposable.add(d);
        }
    }

    private void processNewCarDetails(CarDetails carDetails) {
        CarDetailsViewModel.this.carDetails = carDetails;
        carDetailsLiveData.setValue(carDetails);
        rentIsPossible.setValue(carDetails != null);
    }

    void rentACar() {
        isLoading.setValue(true);
        Disposable d = new CarsRepository().rentCar(new RentCarRequest(carDetails.getCarId()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<RentCarResponse>() {
                    @Override
                    public void accept(RentCarResponse carDetails) {
                        logger.debug("rentACar.onSuccess");
                        isLoading.setValue(false);
                        rideStarted.setValue(true);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        logger.error("rent is not started " + throwable.getMessage());
                        isLoading.setValue(false);
                        rideStarted.setValue(false);
                    }
                });
        destroyDisposable.add(d);
    }

}
