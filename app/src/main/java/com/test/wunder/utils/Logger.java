package com.test.wunder.utils;

import android.util.Log;

public class Logger {
    private String tag;

    public Logger(Class clazz) {
        int maxLength = 23;
        String className = clazz.getSimpleName();
        if(className.length() < 23) {
            tag = className;
        } else {
            tag = className.substring(0, maxLength);
        }
    }

    public void debug(String msg) {
        Log.d(tag, msg);
    }

    public void error(String msg) {
        Log.e(tag, msg);
    }

    public void error(String msg, Throwable t) {
        Log.e(tag, msg + " " + t);
    }

    public void warn(String msg) {
        Log.w(tag, msg);
    }
}
