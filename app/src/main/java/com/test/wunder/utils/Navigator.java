package com.test.wunder.utils;

import android.content.Intent;

import com.test.wunder.screens.cardetails.CarDetailsActivity;

import androidx.appcompat.app.AppCompatActivity;

public class Navigator {

    public static void navigateToCarDetails(AppCompatActivity activity, int carId) {
        Intent intent = new Intent(activity, CarDetailsActivity.class);
        intent.putExtra(CarDetailsActivity.carIdKey, carId);
        activity.startActivity(intent);
    }
}
