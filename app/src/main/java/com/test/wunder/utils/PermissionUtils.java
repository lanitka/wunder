package com.test.wunder.utils;

import android.Manifest;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.test.wunder.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class PermissionUtils {

    public static void askForPermission(View rootView, final AppCompatActivity activity, final int requestCode) {
        if(ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION))
            showLocationNeededSnackbar(rootView, activity, requestCode);
        else
            requestLocationPermission(activity, requestCode);
    }

    private static void requestLocationPermission(AppCompatActivity activity, int requestCode) {
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestCode);
    }

    public static void showLocationNeededSnackbar(View rootView, final AppCompatActivity activity, final int requestCode) {
        Snackbar.make(rootView, R.string.activity_map_request_location_permission_text,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.general_ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestLocationPermission(activity, requestCode);
                    }
                }).show();
    }
}
