Things to improve:
- integrate Dagger to incapsule object creation and enable easy substitution with mock objects for classes like RestCallFactory, different Repositories, ViewModels, etc.
- create a general class-callback for network response processing with generics. So all error processing could be in one place.
- add proper error processing
- use Dagger with scopes to share data between screens. In this test app I passed a selectedCarId in arguments and that could be done using e.g. Repository with a reference to selectedCar.

Dear reviewer,
I would be grateful for your feedback or some tips how this app could be improved, so I could apply your recommendations in future code.
Thank you!

P.S. I ran the app only in debug mode. The map key is not filled for a release build.